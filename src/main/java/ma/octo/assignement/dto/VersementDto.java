package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ma.octo.assignement.domain.Compte;

public class VersementDto {
	
	private BigDecimal montantVersement;
	private String nom_prenom_emetteur;
	private String rib;
	private String motifVersement;
	private Date dateExecution;
	
	public BigDecimal getMontantVersement() {
		return montantVersement;
	}
	public void setMontantVersement(BigDecimal montantVersement) {
		this.montantVersement = montantVersement;
	}
	public String getNom_prenom_emetteur() {
		return nom_prenom_emetteur;
	}
	public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
		this.nom_prenom_emetteur = nom_prenom_emetteur;
	}
	public String getRib() {
		return rib;
	}
	public void setRib(String rib) {
		this.rib = rib;
	}
	public String getMotifVersement() {
		return motifVersement;
	}
	public void setMotifVersement(String motifVersement) {
		this.motifVersement = motifVersement;
	}
	public Date getDateExecution() {
		return dateExecution;
	}
	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}

	
}

package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;

@Service
@Transactional
public class VersementService {

	@Autowired
	private VersementRepository versementRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private AuditService monService;
	
	Logger LOGGER = LoggerFactory.getLogger(VirementService.class);
    public static final int MONTANT_MAXIMAL = 10000;
	
	public List<Versement> loadAll() {
        List<Versement> all = versementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }
	
	
	 public Versement createVersement(VersementDto versementDto)
	            throws CompteNonExistantException, TransactionException {
	        Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRib());

	        if (compteBeneficiaire == null) {
	            System.out.println("Compte Non existant");
	            throw new CompteNonExistantException("Compte Non existant");
	        }

	        if (versementDto.getMontantVersement().equals(null)) {
	            System.out.println("Montant vide");
	            throw new TransactionException("Montant vide");
	        } else if (versementDto.getMontantVersement().intValue() == 0) {
	            System.out.println("Montant vide");
	            throw new TransactionException("Montant vide");
	        } else if (versementDto.getMontantVersement().intValue() < 10) {
	            System.out.println("Montant minimal de versement non atteint");
	            throw new TransactionException("Montant minimal de versement non atteint");
	        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
	            System.out.println("Montant maximal de versement dépassé");
	            throw new TransactionException("Montant maximal de versement dépassé");
	        }

	        if (versementDto.getMotifVersement().length() < 0) {
	            System.out.println("Motif vide");
	            throw new TransactionException("Motif vide");
	        }

	        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
	        compteRepository.save(compteBeneficiaire);

	        Versement versement = new Versement();
	        versement.setDateExecution(new Date());
	        versement.setMotifVersement(versementDto.getMotifVersement());
	        versement.setCompteBeneficiaire(compteBeneficiaire);
	        versement.setMontantVersement(versementDto.getMontantVersement());
	        versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());

	        versementRepository.save(versement);

	        monService.auditVersement("Versement depuis M.(Mme) " + versementDto.getNom_prenom_emetteur() + " vers " + compteBeneficiaire.getNrCompte() + " d'un montant de " + versementDto.getMontantVersement()
	                        .toString());
	        return versementRepository.save(versement);
	    }
}

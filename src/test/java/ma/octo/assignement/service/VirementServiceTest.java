package ma.octo.assignement.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class VirementServiceTest {
	
    @InjectMocks
	private VirementService virementService;
	
	@MockBean
	private VirementRepository virementRepository;
	
	@MockBean
	private UtilisateurRepository utilisateurRepo;
	
	@MockBean
	private CompteRepository compteRepo;
	
	@Test
	void testCreateTransaction() {
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");
		
		utilisateurRepo.save(utilisateur1);
		
		
		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");
		
		utilisateurRepo.save(utilisateur2);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);
		
		compteRepo.save(compte1);
		
		Compte compte2 = new Compte();
		compte2.setNrCompte("010000A000C11000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(300000L));
		compte2.setUtilisateur(utilisateur2);
		
		compteRepo.save(compte2);
		
		VirementDto v = new VirementDto();
		v.setMontantVirement(BigDecimal.TEN);
		v.setNrCompteBeneficiaire(compte2.getNrCompte());
		v.setNrCompteEmetteur(compte1.getNrCompte());
		v.setDate(new Date());
		v.setMotif("Assignment 2021");
		
		Virement virement = new Virement();
        virement.setDateExecution(new Date());
        virement.setMotifVirement(v.getMotif());
        virement.setCompteBeneficiaire(compte1);
        virement.setCompteEmetteur(compte2);
        virement.setMontantVirement(v.getMontantVirement());
        
        
		
		Mockito.when(virementRepository.save(virement)).thenReturn(virement);
				
		try {
			assertThat(virementService.createTransaction(v)).isEqualTo(virement);
		} catch (SoldeDisponibleInsuffisantException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CompteNonExistantException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransactionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void testLoadAll() {
		
	}


}

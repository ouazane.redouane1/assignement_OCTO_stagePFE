package ma.octo.assignement.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class VersementServiceTest {
	
    @InjectMocks
	private VersementService versementService;
	
	@MockBean
	private VersementRepository versementRepository;
	
	@MockBean
	private UtilisateurRepository utilisateurRepo;
	
	@MockBean
	private CompteRepository compteRepo;
	
	@Test
	void testCreateTransaction() {
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");
		
		utilisateurRepo.save(utilisateur1);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);
		
		compteRepo.save(compte1);
		
		VersementDto v = new VersementDto();
		v.setMontantVersement(BigDecimal.valueOf(2000));
		v.setRib(compte1.getRib());
		v.setNom_prenom_emetteur("nome de l'emetteur");
		v.setDateExecution(new Date());
		v.setMotifVersement("Assignment 2021");
		
		Versement versement = new Versement();
        versement.setDateExecution(new Date());
        versement.setMotifVersement(v.getMotifVersement());
        versement.setCompteBeneficiaire(compte1);
        versement.setMontantVersement(v.getMontantVersement());
        
		
		Mockito.when(versementRepository.save(versement)).thenReturn(versement);
				
		try {
			assertThat(versementService.createVersement(v)).isEqualTo(versement);
		} catch (CompteNonExistantException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransactionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Test
	void testLoadAll() {
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Female");
		
		utilisateurRepo.save(utilisateur1);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);
		
		//compteRepo.save(compte1);
		
		List<Versement> versements = new ArrayList<>();
		Versement v = new Versement();
		v.setId(3L);
		v.setCompteBeneficiaire(compte1);;
		v.setMontantVersement(BigDecimal.valueOf(1200));
		v.setNom_prenom_emetteur("Emetteur");
		v.setMotifVersement("Motif");
		
		versements.add(v);
		doReturn(versements).when(versementRepository).findAll();

		List<Versement> result = versementService.loadAll();
		
		assertThat(result).isNotEmpty();
		verify(versementRepository,times(1)).findAll();
	}

}

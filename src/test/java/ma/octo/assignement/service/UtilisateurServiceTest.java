package ma.octo.assignement.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.*;
import ma.octo.assignement.domain.Utilisateur;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ma.octo.assignement.repository.UtilisateurRepository;

@ExtendWith(MockitoExtension.class)
class UtilisateurServiceTest {
	
	@InjectMocks
	private UtilisateurService utilisateurService;

	@Mock
	private UtilisateurRepository  utilisateurRepo;
	
	@Test
	void testLoadAllUtilisateur() {
		
		List<Utilisateur> users = new ArrayList<>();
		Utilisateur user = new Utilisateur();
		user.setId(3L);
		user.setFirstname("first name");
		user.setLastname("last name");
		user.setUsername("user name");
		
		users.add(user);
		doReturn(users).when(utilisateurRepo).findAll();

		List<Utilisateur> result = utilisateurService.loadAllUtilisateur();
		
		//test
		assertThat(result).isNotEmpty();
		verify(utilisateurRepo,times(1)).findAll();
		
	}

}
